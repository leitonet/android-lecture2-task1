package com.example.l2task1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    public EditText name;
    EditText birthday;
    Button confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.name);
        birthday = (EditText) findViewById(R.id.birthday);

        confirm = (Button) findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, HowMatchDays.class);
                if (!name.getText().toString().isEmpty() && !birthday.getText().toString().isEmpty()){
                    intent.putExtra("name", name.getText().toString());
                    intent.putExtra("birthday", birthday.getText().toString());
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, "Information about YOU!", Toast.LENGTH_SHORT).show();

                }

            }
        });

    }
}
