package com.example.l2task1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class HowMatchDays extends AppCompatActivity {

    TextView information;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_match_days);

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        calendar.setTime(new Date());
        String [] arrayOfDate = getIntent().getCharSequenceExtra("birthday").toString().split(".");
        int ageInYears = calendar.get(Calendar.YEAR) - Integer.parseInt(arrayOfDate[2]);
        int ageInDays = calendar.get(Calendar.DAY_OF_YEAR);
        information = (TextView) findViewById(R.id.information);
        information.setText("Hello, " + getIntent().getCharSequenceExtra("name") + " .Your birthday is: " + getIntent().getCharSequenceExtra("birthday") +
        " Your age is - " + ageInYears + " years. ");

    }
}
